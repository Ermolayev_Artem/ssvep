import sys
from psychopy import visual, core, event, data, gui,logging, prefs
from pylsl import StreamInfo, StreamOutlet
import matplotlib.pyplot as plt

import constants

class Window():
    """Graphical window with SSVEP stimuli and optional video backbround

        Args:
            namespace (multiprocessing.managers.NamespaceProxy): common namespace
                for interprocess communication
            video (str, optional): path to video file to display as background.
                Defaults to None.
            plot_intervals (bool, optional): if True, display graph of inter-frame
                intervals before exiting. Useful for debugging. Defaults to False.
    """
    def __init__(self, namespace=None, video:str=None, plot_intervals:bool=False):
        self.namespace = namespace
        self.plot_intervals = plot_intervals
        self.namespace.PAUSE = False
        self.Fullscreen = False

        self.create_psychopy_objects(video)
        core.wait(0.1)

    def create_psychopy_objects(self, video:str):
        """Pre-allocate stimuli and other objects to be used for stimulation

        Args:
            video (str, optional): path to video file to display as background.
                Defaults to None.
        """
        self.win = visual.Window(fullscr = False,
                            rgb = constants.background,
                            size = constants.window_size,
                            winType = 'pyglet')
        self.win.recordFrameIntervals = True
        if video:
            self.video = visual.MovieStim3(self.win, filename = video, loop=True, noAudio = True)
        else:
            self.video = None

        self.stims = [self.draw_shape(stim_id, obj_data)
                      for stim_id, obj_data in constants.stim.items()
                      ]
        self.stim_frames_counter = {k: 0 for k in constants.stim.keys()}

    def draw_shape(self, stim_id: float, obj_data: dict):
        """Draw specified shape

        Args:
            stim_id: shape name
            obj_data: additional data
        """
        shape_type = obj_data['shape']
        if shape_type == 'circle':
            return visual.Circle(self.win, lineColor='white', fillColor=constants.colors[0], pos=obj_data['pos'],
                        size=constants.size, autoDraw=True, units='pix', name=stim_id)
        elif shape_type == 'square':
            return visual.Rect(self.win, lineColor='white', fillColor=constants.colors[0], pos=obj_data['pos'],
                        size=constants.size, ori=obj_data['ori'], autoDraw=True, units='pix', name=stim_id)
        elif shape_type == 'stop':
            t = visual.Circle(self.win, lineColor='white', fillColor=constants.colors[0], pos=obj_data['pos'],
                        size=constants.size, ori=obj_data['ori'], autoDraw=True, units='pix', name=stim_id)
            # visual.Rect(self.win, width=0.3, height=0.1, lineColor='white', fillColor='white', pos=obj_data['pos'],
            #             size=constants.size, ori=obj_data['ori'], autoDraw=True, units='pix', name=None)
            t2 = visual.TextStim(self.win, text='STOP', color='white', pos=obj_data['pos'], units='pix')
            t2.setAutoDraw(True)
            return t
        elif shape_type == 'shape':
            return visual.ShapeStim(self.win, lineColor='white', fillColor=constants.colors[0], vertices=obj_data['vertices'],
                        pos=obj_data['pos'], size=constants.size, ori=obj_data['ori'], autoDraw=True, units='pix', name=stim_id)
        else:
            assert False, 'error'

    def draw_stim(self, stim:visual.BaseVisualStim):
        """Change stimuli state according to current bit of stimulation sequence.

        Args:
            stim (visual.BaseVisualStim): stimuli instance
        """
        key = self.stim_frames_counter[stim.name] % len(constants.stim[stim.name]['duty_cicle'])
        bit = constants.stim[stim.name]['duty_cicle'][key]
        stim.fillColor = constants.colors[bit]
        self.stim_frames_counter[stim.name] += 1
    
    def _exit(self):
        """Close stimulation window. If it was created with show_intervals=True,
                display frame intervals graph.
        """
        self.win.close()
        if self.plot_intervals:
            plt.plot(self.win.frameIntervals, 'o')
            plt.show()
        sys.exit()
    
    def run(self):
        """Main cycle of the programm. Monitors for keyboard shortcuts and namespace
                flags, updates stimuli and optional video background frame-by-frame,
                sends screen refresh signal.
        """
        while 1:
            k = event.getKeys()
            if 'escape' in k:
                self._exit()
            elif 'p' in k:
                self.namespace.PAUSE = self.namespace.PAUSE == False
            if hasattr(self.namespace, 'KILL'):
                self._exit()
            if self.video:
                self.video.draw()
            if not self.namespace.PAUSE:
                [self.draw_stim(stim) for stim in self.stims]
            self.win.flip()


if __name__ == '__main__':
    w =  Window(video=r".\resources\car_video.mp4", plot_intervals=False, namespace=constants.namespace)
    # w =  Window(namespace=constants.namespace)
    w.run()