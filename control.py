import sys, time
from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtCore import *
import pylsl
import constants

class loop_thread(QThread):
    """When running, the thread quieries classification every 
        constants.classification_timeout seconds
    """
    def __init__(self, namespace):
        QThread.__init__(self)
        self.namespace = namespace
    def run(self):
        """Set the flag to launch classification, pause the stimulation
            for one second, wait for constants.classification_timeout seconds,
            repead
        """
        while 1:
            print('classify')
            self.namespace.CLASSIFY = True
            self.namespace.PAUSE = True
            time.sleep(1)
            self.namespace.PAUSE = False
            time.sleep(constants.classification_timeout)

class Window(QWidget):
    """Main GUI window to manually control the classification
    """
    def __init__(self, namespace):
        super(QWidget, self).__init__()
        self.namespace = namespace
        self.layout = QVBoxLayout()
        self.setLayout(self.layout)
        self._initUI()

    def _initUI(self):
        btn = QPushButton('stop', self)
        btn2 = QPushButton('classify', self)
        self.btn3 = QPushButton('classify_loop', self)

        self.layout.addWidget(btn)
        self.layout.addWidget(btn2)
        self.layout.addWidget(self.btn3)

        btn.clicked.connect(self.stop_record)
        btn2.clicked.connect(self.classify)
        self.btn3.setCheckable(True)
        self.btn3.clicked.connect(self.classify_loop)

        self.setWindowTitle('BCI control')
        self.setGeometry(50,50,300,150)
        self.show()

    def stop_record(self):
        """Set exit flag. Any process seeing this flag will try to save the data
            and exit
        """
        print ('sending exit signal')
        self.namespace.KILL = True
        sys.exit()

    def classify(self):
        """Set classify flag. When the classifier process sees this flag, the
            classification pipeline is initiated.
        """
        print ('classify')
        self.namespace.CLASSIFY = True

    def classify_loop(self):
        """While the classify_loop button is pressed, the classification pipeline
            will be initiated every constants.classification_timeout seconds.
        """
        if self.btn3.isChecked():
            self.thread = loop_thread(self.namespace)
            self.thread.start()
        else:
            self.thread.terminate()

def main(namespace):
    app = QApplication(sys.argv)
    ex = Window(namespace=namespace)
    sys.exit(app.exec_())

if __name__ == '__main__':
    main(namespace=constants.namespace)
