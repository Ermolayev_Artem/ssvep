classify_signal = 888
kill_signal = 999

# EEG recording params
mapnames = {'eeg':'./eegdata.mmap'} # path to memory mapped file
data_folder = './data' # path where to save the data

stream_type_eeg = 'Data' # NeoRec EEG data stream type
top_exp_length = 30 # maximum experiment length in minutes to allocate memory map array for
last_eeg_chunk_len = 500 # how many samples to receive after shutdown signal before exiting

timestring_format = '%H_%M__%d_%m' # time string format for file names

espeak_path = r"C:\Program Files (x86)\eSpeak\command_line\espeak.exe" # install Espeak for spoken feedback

#classifier settings
window = 3000 # window to use for classification
classification_timeout = 5 # how often to invoke classification routine in the loop mode

# stimuli params
video = None # If not None, path to the video files to use as background
# video = r".\resources\cat.mp4" # If not None, path to the video files to use as background
background = 'black'
window_size = [1800,1000]
colors = ['red', 'green'] # which colors to stimulate with. Colors are encoded as [0,1]
size = 120 # stimuli size in pixels

"""Stimulation cycle represented in display refresh frames.

{
    frequency1:[color for frame 1 (fr0k constant.colors),
            color for frame 2,
            ...
            color for last frame in the cycle],
    frequency for stimuli 2: [...],
    ...
     frequency for the last stimuli: [...]
}

To change the frequency you need to change both the key and values correspondingly.
for details on SSVEP stimulation cycles see
Volosyak I, Cecotti H, Graser A. Optimal visual stimuli on LCD screens for 
SSVEP based Brain-Computer Interfaces. In2009 4th International IEEE/EMBS 
Conference on Neural Engineering 2009 Apr 29 (pp. 447-450). IEEE.
"""
# stim_frames = {
#                 5: [0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1],
#                 7.5: [0, 0, 0, 0, 1, 1, 1, 1],
#                 8.57: [0, 0, 0, 0, 1, 1, 1],
#                 10: [0, 0, 0, 1, 1, 1],
#                 12: [0, 0, 0, 1, 1],
#                 }

# # stimuli positions in pixels
# stim_positions = {
#                 5: [-800,0],
#                 7.5: [-400,0],
#                 8.57: [0,0],
#                 10: [400,0],
#                 12: [800,0],
#                 }

arrow_vertices = ((-1,0),(0,1),(1,0),(0.5,0),(0.5,-1),(-0.5,-1),(-0.5,0))



stim = {
    5:
    {
        'duty_cicle': [0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1],
        'pos': (-800,0),
        'shape': 'shape',
        'vertices': arrow_vertices,
        'ori':-90
    },
    7.5:
    {
        'duty_cicle': [0, 0, 0, 0, 1, 1, 1, 1],
        'pos': (-400,0),
        'shape': 'shape',
        'vertices': arrow_vertices,
        'ori':180
    },
    8.57:
    {
        'duty_cicle': [0, 0, 0, 0, 1, 1, 1],
        'pos': (0,0),
        'shape': 'stop',
        'ori':0
    },
    10:
    {
        'duty_cicle': [0, 0, 0, 1, 1, 1],
        'pos': (400,0),
        'shape': 'shape',
        'vertices': arrow_vertices,
        'ori':0
    },
    12:
    {
        'duty_cicle': [0, 0, 0, 1, 1],
        'pos': (800,0),
        'shape': 'shape',
        'vertices': arrow_vertices,
        'ori':90
    }
}

demo=True

                
class namespace:
    """Empty classs for compatibility with namespace system, so the process can 
        run wihtout other processes
    """
    pass