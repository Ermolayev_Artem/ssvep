import multiprocessing, sys, os, time, argparse
import control, stimuli, eeg
import logging
import constants

def data(namespace):
    """Launch data acquisition in separate process

    Args:
        namespace (multiprocessing.managers.NamespaceProxy): common namespace
            for interprocess communication
    """
    stream = eeg.EEG_STREAM(namespace=namespace)
    stream.mainloop()

def gui(namespace):
    """Launch control GUI window in separate process

    Args:
        namespace (multiprocessing.managers.NamespaceProxy): common namespace
            for interprocess communication
    """
    control.main(namespace)

def stims(namespace):
    """Launch stimulation window in separate process

    Args:
        namespace (multiprocessing.managers.NamespaceProxy): common namespace
            for interprocess communication
    """
    w =  stimuli.Window(namespace, video=constants.video)
    w.run()

if __name__ == '__main__':
    # Create common namespace object
    mgr = multiprocessing.Manager()
    namespace = mgr.Namespace()

    processes=[]
    
    # Create processes for online BCI
    processes.append( multiprocessing.Process(target=stims, args=(namespace,)))
    if not constants.demo:
        processes.append( multiprocessing.Process(target=data, args=(namespace,)))
    processes.append( multiprocessing.Process(target=gui, args=(namespace,)))

    # Start processes
    for proc in processes:
        proc.start()
    
    for proc in processes:
        proc.join()
    
