import pylsl
from scipy import signal
import numpy as np
import sys, os, warnings, time, datetime

import constants
import classifier

class EEG_STREAM(object):
    """Main object, dealing with receiving EEG samples, cutting EEG and saving data

    Args:
        namespace (multiprocessing.managers.NamespaceProxy): common namespace
            for interprocess communication
    """
    def __init__(self, namespace):
        self.namespace = namespace
        self.stop = False  # set EEG_STREAM.stop to 1 to flush arrays to disc. This variable is also used to choose the exact time to stop record.
        self.ie = self.create_eeg_stream()
        
        print ('EEG stream contains {} channels'.format(self.ie.channel_count))
        self.EEG_ARRAY = self.allocate_array(mmapname=constants.mapnames['eeg'],
            top_exp_length=constants.top_exp_length,
            number_of_channels=self.ie.channel_count)
        self.event_timestamps = []
        self.namespace.created_array = True

    def create_eeg_stream(self):
        """Create LSL inlet for EEG data

        Returns:
            pylsl.StreamInlet: Incoming LSL stream object
        """
        print ("Connecting to eeg stream...")
        stream_eeg = pylsl.resolve_stream('type', constants.stream_type_eeg)
        inlet_eeg = pylsl.StreamInlet(stream_eeg[0], recover=True)
        self.sampling_rate = stream_eeg[0].nominal_srate()
        print ('...done')
        return inlet_eeg

    def allocate_array(self, mmapname:str, dtype:str='float', top_exp_length:int=60, number_of_channels:int=8):
        """Preallocate EEG array for whole experiment.

        Args:
            mmapname (str): path to preallocated memory map array
            dtype (str, optional): Data type for the array. Defaults to 'float'.
            top_exp_length (int, optional): maximum predicted experiment length 
                in minutes. Defaults to 60.
            number_of_channels (int, optional): Number of EEG channels to allocate.
                Normally is recieved from pylsl.StreamInlet object info. Defaults to 8. 

        Returns:
            np.ndarray: Preallocated preallocated memory map array
        """
        record_length = self.sampling_rate*60*top_exp_length*1.2
        array_shape = (int(record_length), number_of_channels+1)
        print ('Creating array with dimensions %s...' %str(array_shape))
        preallocated_array = np.memmap(mmapname, dtype=dtype, mode='w+', shape=array_shape)
        preallocated_array[:,0:self.ie.channel_count+1] = np.NAN
        self.samples_recieved = 0
        print ('... done')
        return preallocated_array

    def fill_eeg_array(self, data_chunk:np.ndarray, timestamp_chunk:np.ndarray):
        """IF nonempty chunk of data and/or timestamps are received, place them
            into EEG array.

        Args:
            data_chunk (np.ndarray): EEG samples chunk from LSL inlet
            timestamp_chunk (np.ndarray): timestamps chunk from LSL inlet
        """        
        if timestamp_chunk:
            self.EEG_ARRAY[self.samples_recieved:self.samples_recieved+len(timestamp_chunk), 0] = timestamp_chunk
        if data_chunk:
            self.EEG_ARRAY[self.samples_recieved:self.samples_recieved+len(data_chunk),1:] = data_chunk

    def remove_nans_from_eeg(self):
        """Before saving EEG array, remove all unused NaNs in the end of array.

        Returns:
            np.ndarray: Clean EEG array
        """
        with warnings.catch_warnings(): # >< operators generate warnings on arrays with NaNs, like our EEG array
            warnings.simplefilter("ignore")
            eeg = self.EEG_ARRAY[np.logical_and((self.EEG_ARRAY[:,0]>0),
                (np.isnan(self.EEG_ARRAY[:,1]) != True)),:]
            return eeg

    def save_data(self):
        """Clean EEG array and save it. Data and events are saved separately, and
            curent local timestamp is embedded in the filename. Files will be saved
            to constants.data_folder. If data folder does not exist, it'll be created.
        """
        print ('\nsaving experiment data...\n')
        eegdata = self.remove_nans_from_eeg()
        file_id = timestring()
        
        if not os.path.exists(constants.data_folder):
            os.makedirs(constants.data_folder)

        np.save(os.path.join(constants.data_folder, f'data_{file_id}.npy'), eegdata)
        np.save(os.path.join(constants.data_folder, f'events_{file_id}.npy'), self.event_timestamps)

    def mainloop(self):
        """Main loop for dealing with EEG stream. The loop checks for namespace
            flags and is able to get EEG samples from the stream, add samples to
            the memory mapped array, initiate classification routine, save the 
            recorted data to the disc.
        """
        self.namespace.EEG_RECORDING_STARTED = True
        while 1:
            if hasattr(self.namespace, 'KILL'):
                if not hasattr(self.namespace, 'KILL_RECEIVED'):
                    self.namespace.KILL_RECEIVED = True
                    print ('received shutdown signal')
                    self.stop = self.samples_recieved + constants.last_eeg_chunk_len
            if hasattr(self.namespace, 'CLASSIFY') and self.namespace.CLASSIFY:
                self.namespace.CLASSIFY = False
                eeg =self.EEG_ARRAY[self.samples_recieved - constants.window:self.samples_recieved,:]
                if np.max(eeg.shape) >= constants.window:
                    timestamp_start = eeg[0,0]
                    self.event_timestamps.append(timestamp_start)
                    eeg = eeg[:,1:]
                    print (timestamp_start)
                    try:
                        result = classifier.classifier_endpoint(data = eeg,
                            freqs = list(constants.stim.keys()),
                            window = constants.window,
                            sampling_rate = self.sampling_rate)
                        print (f"classification result {result}")
                    except Exception as e:
                        print (f'ERROR: {e}')
                else:
                    print (f'not enough EEG, received {np.max(eeg.shape)} samples, need at least {constants.window}')
            try:
                EEG, timestamp_eeg = self.ie.pull_chunk()
            except Exception as e:
                print (e)
                EEG, timestamp_eeg = [], []
            
            if timestamp_eeg:
                self.fill_eeg_array(EEG, timestamp_eeg)
                self.samples_recieved += len(timestamp_eeg)
                
                if self.stop != False : # save last EEG chunk before exit
                    if self.samples_recieved >= self.stop:
                        self.save_data()
                        print ('\n...data saved.\n Goodbye.\n')
                        sys.exit()

def timestring():
    """Return formatted local time to be used in filenames.

    Returns:
        str: formatted local time
    """
    return datetime.datetime.fromtimestamp(time.time()).strftime(constants.timestring_format)

if __name__ == '__main__':
    Stream = EEG_STREAM(namespace = constants.namespace)
    Stream.mainloop()