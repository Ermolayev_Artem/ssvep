# Prototype SSVEP BCI

This is a prototype SSVEP BCI with variable stimuli parameters.

This software prototype was developed at Lomonosov Moscow State University by Rafael Grigoryan and others\
Contact: grk@eegdude.ru

### About this programm

This programm contains several independent modules. These modules are running in the separate Python proceess to achieve real-time performance and ~ 1 ms temporal precision. Interprocess communication is organised via common namespace, provided in builtin multiprocessing library.
It's possible to display several SSVEP stimuli on the screen, and to set up different positions, duty cycles, colors and frequencies. It's also possible to play video clips at the background.

| file | Description |
| - | - |
| control.py | GUI control module |
| eeg.py | EEG recording module |
| classifier.py | Classification module |
| stimuli.py | graphical module with SSVEP stimuli |
| laucher.py | the script to launch classifier, stimulation window, control window and eeg recorder in separate processes |
| constants.py | The configuration file with all the needed settings |
| rcca.py | regularised canonical correlation analysis core, developed by Gallant lab, UC Berkley |
| Analysis.ipynb | Jupyuter notebook for testing the classifier on recorded data |

### Requirements

* Python 3.7.6
* Packages as specified in requirements.txt
* Windows 10
* Discrete GPU
* NeoRec 1.4.9 and newer *
* optional Espeak for voice feedback (http://espeak.sourceforge.net/)

*This software was tested with NVX-52 EEG device from MCS Ltd (Zelenograd, Russia). However, most EEG devices would probably work, if they are capable of sending synchronised stream of data over network via LSL protocol.

### Testing
* Install all requirements. Using virtualenv or Anaconda is preferred.
* Set up EEG
* Start NeoRec, set up the LSL stream (see NeoRec documentaion for the details)
* Check constants.py file to set up parameters of stimulation
* Run laucher.py file. If several GPUs are present in the system, make sure that Python interpreter is running wihth discreted GPU (for Nvidia graphics, this can be set from Nvidia control panel (Manage 3D settings - Program settings)).
* Check console log for errors
* Stimulation window will open along with control GUI
* Control the stimulation and classification using BCI control gui

    ![](bci_control_gui.png)

    * stop - stop the record, save the data and exit

    * classify - classify the last chunk of the data

    * classify_loop - continious classification with intervals defined at constants.py, as long as the button is checked

* you can also control the experiment from stimulation window.

    * Use P to pause and unpause the stimulation

    * Use Esc to exit the programm and save the data

### Data description

Recorded ata is saved at folder, specified in constants.py file. 

Data structure for each session:

| filename | description | array shape |
| - | - | - |
| data_$localtime$.npy | EEG array | (n_samples X (time + n_channels)) |
| events_$localtime$.npy | array of time points, corresponding to the start of each target selection cycle | (N targets) |