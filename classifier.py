import numpy as np
import os, sys, time, warnings, ast, itertools
import subprocess

from scipy import signal
import rcca

import constants

def create_reference_signals(f:float, sampling_rate:int, window:int,
    number_of_harmonics:int=4):
    """Unsupervised CCA requires reference signal matices for all frequencies.
        This function creates the reference signal of correct shape for selected
        frequency.
        For details, see Hakvoort, Gido, Boris Reuderink, and Michel Obbink.
        "Comparison of PSDA and CCA detection methods in a SSVEP-based BCI-system."
        Centre for Telematics & Information Technology University of Twente (2011).

    Args:
        f (float): Target frequency.
        sampling_rate (int): Samling rate, Hz. Defaults to 500.
        window (int): Length of the reference signal in samples. 
        number_of_harmonics (int, optional): number of harmonics to use for the
            reference signals. Defaults to 4.

    Returns:
        np.ndarray: Reference signal.
    """
    x = np.arange(window)
    refs = []
    for H in range(1, number_of_harmonics+1):
        pair = [np.sin(H*2*np.pi*f*x/sampling_rate),
            np.cos(H*2*np.pi*f*x/sampling_rate)]
        refs.extend(pair)
    return np.array(refs).T

def create_filter_bank(fr:float, sampling_rate:int, n_bands:int=9):
    """Create filters for filter bank canonical correlation analysis.

    Args:
        fr ([type]): target frequency
        sampling_rate ([type]): Samling rate, Hz.
        n_bands (int, optional): Number of filter banks. Defaults to 9.

    Returns:
        list: List with filter objects [band1, band2 ... band[N]]
    """
    nyq = float(0.5 * sampling_rate)
    bands = [[fr*a/nyq, 90/nyq] for a in range(1, n_bands+1) if fr*a<90]  #M3
    filters = [signal.cheby1(4, 1, band, btype = 'bandpass', analog=False) for band in bands]
    return filters

def classify_cca(eeg:np.ndarray, freqs:list, reference_signals:np.ndarray=[]):
    """Run canonical correlation analysis and return selected frequency

    Args:
        eeg (np.ndarray): EEG array
        freqs (list): list of frequencies to test
        reference_signals (np.ndarray, optional): Referece signals for all frequencies.
            Defaults to [].

    Returns:
        float: Selected frequency.
    """
    freq_corr = []
    for (nfr, fr), ref in zip(enumerate(freqs), reference_signals):
        cca = rcca.CCA(kernelcca = False, reg = 0., numCC = 1, verbose = False)
        cca.train([eeg, ref])
        freq_corr.append(cca.cancorrs)
    selected_freq = freqs[np.argmax(freq_corr)]
    return selected_freq

def classify_fbcca(eeg:np.ndarray, freqs:list, reference_signals:np.ndarray=[],
    filter_banks:list=[]):
    """Run filter bank canonical correlation analysis and return selected frequency
        See https://doi.org/10.1088/1741-2560/12/4/046008 for details.
    Args:
        eeg (np.ndarray): EEG array
        freqs (list): list of frequencies to test
        reference_signals (np.ndarray, optional): Referece signals for all frequencies.
            Defaults to [].
        filter_banks (list): list of filters for all frequencies and all bands.

    Returns:
        float: Selected frequency.
    """
    freq_corr_fb = []
    for (nfr, fr), ref in zip(enumerate(freqs), reference_signals):
        freq_corr = []
        filter_bands = filter_banks[nfr]
        for band in filter_bands:
            eeg_filtered = signal.filtfilt(band[0], band[1], eeg, axis = 0)
            cca = rcca.CCA(kernelcca = False, reg = 0., numCC = 1, verbose = False)
            try:
                cca.train([eeg_filtered, ref])
                freq_corr.append(cca.cancorrs[0])
            except	Exception as e:
                print ('ERROR', e)
        freq_corr_fb.append(sum([((N+1)**-2)*(freq_corr[N]**2) for N in range(len(freq_corr))] ))
    selected_freq = freqs[np.argmax(freq_corr_fb)]
    return selected_freq

def classifier_endpoint(data:np.ndarray, freqs:list, window:int, sampling_rate:float,
    voice:bool=True, alg:str='FBCCA'):
    """Main function for classification of EEG data.
        Create reference signal, classify the data with CCA or FBCCA and optionally
        say most probable target frequency.

    Args:
        data (np.ndarray): EEG array
        freqs (list): list of frequencies to test
        window (int): Length of the reference signal in samples.
        sampling_rate (int): Samling rate, Hz.
        voice (bool, optional): Whether to give voice feedback for classification.
            Defaults to True.
        alg (str, optional): Algorithm to classify data with. Currently, CCA and
            FBCCA are implemented. Defaults to 'FBCCA'.

    Returns:
        int|str: selected frequency
    """
    refs = [create_reference_signals(f=fr, window=window, sampling_rate=sampling_rate) for fr in freqs]
    if alg.upper() == 'FBCCA':
        filter_banks = [create_filter_bank(fr, sampling_rate=sampling_rate) for fr in freqs]
        ans = classify_fbcca(data, freqs=freqs, reference_signals=refs, filter_banks=filter_banks)
    elif alg.upper() == 'CCA':
        ans = classify_cca(data, freqs=freqs, reference_signals=refs)
    if voice:
        speak(ans)
    return ans

def speak(word=False):
    """Call Espeak to say stuff.

    Args:
        word (optional): Stuff to say. Needs to be convertable to string. Defaults to False.
    """
    word = str(word)
    try:
        subprocess.call([constants.espeak_path, word])
    except WindowsError:
        print ('install eSpeak for speech synthesis')